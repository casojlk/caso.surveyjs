﻿/* ServiceController - Interface between web page and service 
 * 
 * Edit History
 * jlk 14-feb-2020 1101 Creation event - Rework original to use StorageAccess class for CASO's use.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace surveyjs_aspnet_mvc.Controllers
{
    /* Used when the survey definition is being changed.
     */
    public class ChangeSurveyModel
    {
        public string Id { get; set; } // Really surveyName
        public string Json { get; set; }
        public string Text { get; set; }
    } // end ChangeSurveyModel

    /* Used to get survey results sent by SurveyJS when user
     * completes a survey.
     */
    public class PostSurveyResultModel
    {
        public string postId { get; set; }  // Really surveyName
        public string surveyResult { get; set; }
    } // end PostSurveyResultModel

    [Route("")]
    public class ServiceController : Controller
    {
        /* Get a list of all active survey definitions
         */
        [HttpGet("getActive")]
        public JsonResult GetActive()
        {
            var db = new StorageAccess();
            return Json(db.GetSurveys());
        } // end GetActive

        /* Get a specific servey definition by survey name.
         * 
         * Notes:
         * - surveyID is the name of the survey -- not a number.
         */
        [HttpGet("getSurvey")]
        public string GetSurvey(string surveyId)
        {
            var db = new StorageAccess();
            return db.GetSurvey(surveyId);
        } // end GetSurvey

        /* Create an empty survey definition with a generated, temporary name.
         */
        [HttpGet("create")]
        public JsonResult Create(string name)
        {
            var db = new StorageAccess();
            db.CreateSurvey(name, "\"{}\"");
            return Json("Ok");
        } // end Create

        /* Change the name of a survey definition.
         * - id is the old name.
         * - name is the new name.
         */
        [HttpGet("changeName")]
        public JsonResult ChangeName(string id, string name)
        {
            var db = new StorageAccess();
            db.ChangeName(id, name);
            return Json("Ok");
        } // end ChangeName

        /* Save a survey definition, overwriting the existing definition.
         * - model.Id is the name of the survey.
         * - model.Json is the Json text of the definition from surveyJS.
         */
        [HttpPost("changeJson")]
        public string ChangeJson([FromBody]ChangeSurveyModel model)
        {
            var db = new StorageAccess();
            db.StoreSurvey(model.Id, model.Json);
            return db.GetSurvey(model.Id);
        } // end ChangeJson

        /* Delete a survey defintion.
         * - id the name of the survey defintion.
         */
        [HttpGet("delete")]
        public JsonResult Delete(string id)
        {
            var db = new StorageAccess();
            db.DeleteSurvey(id);
            return Json("Ok");
        } // end Delete

        /* When a survey is run, the user fills it and and clicks Complete.
         * That button calls this entry point with an ID and the results as JSON text.
         */
        [HttpPost("post")]
        public JsonResult PostResult([FromBody]PostSurveyResultModel model)
        {
            var db = new StorageAccess();
            db.StoreResult(model.postId, model.surveyResult);
            return Json("Ok");
        } // end PostResult

        /* Fetch the result files for a specific survey. There may be zero or many.
         * 
         * Notes:
         * - postID is the name of the survey.
         */
        [HttpGet("results")]
        public JsonResult GetResults(string postId)
        {
            var db= new StorageAccess();
            return Json(db.GetResults(postId));
        }  // end GetResults
    } // end ServiceController
} // end namespace surveyjs_aspnet_mvc
