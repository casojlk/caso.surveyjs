## CASO.SurveyJS - A survey forms project
Created 13-feb-2020 by Jon Knox

The source basis for this web project is "github.com/surveyjs/surveyjs-aspnet-mvc".
The initial purpose is to illustrate the integration possibilities and make it a CASO
project. Eventually, this may evolve into a CASO application for other clients. The
first one with probably be Signature Bank.

The code continues to use the SurveyJS editor, but the storage of survey form definitions
has been changed to a data base. The web interface was not modified and the initial capabilities
are those of the model created by SurveyJS. To be useful, that and other things will be changed
by later versions.

This version was initially developed using Visual Studio 2019 which needs .NET Core on the
computer (https://www.microsoft.com/net/download/core).
