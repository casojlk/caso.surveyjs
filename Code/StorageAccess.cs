﻿/* StorageAccess - Provide access to DB for survey and survey results storage.
 * 
 * Edit History
 * jlk  5-Mar-2020 1342 Replace storage of results to a file with storage to DB field.
 *     Also, activate code to delete results records.
 * jlk 14-feb-2020 1101 Creation event - replace use of class SessionStorage for CASO's use
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Text;
using Newtonsoft.Json;
using System.IO;

namespace surveyjs_aspnet_mvc
{
public class StorageAccess
{
    private const string connString_cs = "Server=169.254.76.179;DataBase=SurveyForms;User ID=sysop;Password=sigotg";  // (CASO SBDEV) DB connection string; should come from config file
    private const string logFilePath_cs = "c:\\temp\\SurveyService.log";

    // === Survey operations ===
    
    /* Get all survey definitions from DB.
     */
    public Dictionary<string,string> GetSurveys()
    {
        SqlConnection DB_conn;                      // Database connection
        SqlDataReader listReader_dr;                // Reader for selected survey definitions
        Dictionary<string, string> surveyList_dic;  // Returned survey definitions
        SqlCommand table_cmd;                       // SQL table command

        surveyList_dic = new Dictionary<string, string>();
        DB_conn = new SqlConnection(connString_cs);
        DB_conn.Open();

        table_cmd = new SqlCommand("SELECT FormName, Definition FROM Forms", DB_conn); // Need to get FormID too and use
        listReader_dr = table_cmd.ExecuteReader();
        while (listReader_dr.Read())
            surveyList_dic.Add(listReader_dr.GetString(0), listReader_dr.GetString(1)); // **** DON'T REALLY NEED DEFINITION

        listReader_dr.Close();
        table_cmd.Dispose();
        DB_conn.Close();
        DB_conn.Dispose();
        return surveyList_dic;
    } // end GetSurveys

    /* Get specific survey definition from DB
     */
    public string GetSurvey(string surveyName_s)
    {
        SqlConnection DB_conn;          // Database connection
        SqlDataReader listReader_dr;    // Reader for the selected survey definition
        string surveyDef;               // Returned survey definitions
        SqlCommand table_cmd;           // SQL table command

        DB_conn = new SqlConnection(connString_cs);
        DB_conn.Open();

        table_cmd = new SqlCommand(String.Format("SELECT Definition FROM Forms WHERE FormName = '{0}'", surveyName_s), DB_conn);
        listReader_dr = table_cmd.ExecuteReader();
        if (! listReader_dr.HasRows)
            surveyDef = "\"{}\"";
        else
        { 
            listReader_dr.Read();
            surveyDef = JsonConvert.DeserializeObject<string>(listReader_dr.GetString(0));
        }

        listReader_dr.Close();
        table_cmd.Dispose();
        DB_conn.Close();
        DB_conn.Dispose();
        return surveyDef;
    } // end GetSurvey

    /* Store new survey definition in DB
     */
    public void CreateSurvey(string surveyName_s, string jsonSurveyDef_s)
    {
        SqlConnection DB_conn;          // Database connection
        string insertCmd_s;             // Generated SQL INSERT command
        SqlCommand table_cmd;           // SQL table command

        DB_conn = new SqlConnection(connString_cs);
        DB_conn.Open();
        insertCmd_s = String.Format("INSERT INTO Forms (FormName, Definition, Created) VALUES ('{0}','{1}','{2:yyyy-MM-dd HH:mm}')",
            surveyName_s, JsonConvert.SerializeObject(jsonSurveyDef_s), DateTime.Now);
        table_cmd = new SqlCommand(insertCmd_s, DB_conn);
        table_cmd.ExecuteNonQuery();

        table_cmd.Dispose();
        DB_conn.Close();
        DB_conn.Dispose();
    } // end CreateSurvey

    /* Store existing survey definition in DB
     */
    public void StoreSurvey(string surveyName_s, string jsonSurveyDef_s)
    {
        SqlConnection DB_conn;          // Database connection
        string insertCmd_s;             // Generated SQL INSERT command
        SqlCommand table_cmd;           // SQL table command

        DB_conn = new SqlConnection(connString_cs);
        DB_conn.Open();
        insertCmd_s = String.Format("UPDATE Forms SET Definition = '{0}' WHERE FormName ='{1}'",
            JsonConvert.SerializeObject(jsonSurveyDef_s), surveyName_s);
        table_cmd = new SqlCommand(insertCmd_s, DB_conn);
        table_cmd.ExecuteNonQuery();

        table_cmd.Dispose();
        DB_conn.Close();
        DB_conn.Dispose();
    } // end StoreSurvey

    /* Change the name of a survey definition
     */
    public void ChangeName(string oldName_s, string newName_s)
    {
        SqlConnection DB_conn;          // Database connection
        SqlCommand table_cmd;           // SQL table command
        string updateCmd_s;             // Generated SQL UPDATE command

        DB_conn = new SqlConnection(connString_cs);
        DB_conn.Open();
        updateCmd_s = String.Format("UPDATE Forms SET FormName = '{0}' WHERE FormName = '{1}'", newName_s, oldName_s);
        table_cmd = new SqlCommand(updateCmd_s, DB_conn);
        table_cmd.ExecuteNonQuery();

        table_cmd.Dispose();
        DB_conn.Close();
        DB_conn.Dispose();
    } // end ChangeName

    /* Delete a survey definition and associated results.
     * Caller should have already confirmed deletion.
     */
    public void DeleteSurvey(string surveyName_s)
    {
        SqlConnection DB_conn;          // Database connection
        int formID_i;                   // Form ID of survey to delete
        string sqlCmd_s;                // Generated SQL SELECT command
        SqlCommand table_cmd;           // SQL table command

        DB_conn = new SqlConnection(connString_cs);
        DB_conn.Open();
        sqlCmd_s = String.Format("SELECT formID FROM Forms WHERE FormName = '{0}'", surveyName_s);
        table_cmd = new SqlCommand(sqlCmd_s, DB_conn);
        formID_i = Convert.ToInt32(table_cmd.ExecuteScalar());

        sqlCmd_s = String.Format("DELETE FROM Forms WHERE FormID = {0}", formID_i);
        table_cmd = new SqlCommand(sqlCmd_s, DB_conn);
        table_cmd.ExecuteNonQuery();
        table_cmd.Dispose();

        sqlCmd_s = String.Format("DELETE FROM Results WHERE FormID = {0}", formID_i);
        table_cmd = new SqlCommand(sqlCmd_s, DB_conn);
        table_cmd.ExecuteNonQuery();
        
        table_cmd.Dispose();
        DB_conn.Close();
        DB_conn.Dispose();
    } // end DeleteSurvey

    // === Results Operations ===

    /* Store survey result in DB.
     * 
     * Notes:
     * - The survey content is not serialized in the file.
     */
    public void StoreResult(string surveyName_s, string resultJson_s)
    {
        SqlConnection DB_conn;          // Database connection
        int formID_i;                   // Database form ID
        int resultID_i;                 // Survey result ID/key of added Results record
        byte[] dataStream_ay;         // JSON result converted to byte array
        string sqlCmd_s;                // Generated SQL SELECT or INSERT command
        SqlCommand table_cmd;           // SQL table command

        DB_conn = new SqlConnection(connString_cs);
        DB_conn.Open();
        sqlCmd_s = String.Format("SELECT FormId FROM Forms WHERE FormName='{0}'", surveyName_s);
        table_cmd = new SqlCommand(sqlCmd_s, DB_conn);
        formID_i = Convert.ToInt32(table_cmd.ExecuteScalar());

        // There has been talk of saving the result as a PDF file, so this field is defined as varbinary.
        dataStream_ay = Encoding.ASCII.GetBytes(resultJson_s);    // Note, this is not modified (serialized)

        sqlCmd_s = String.Format("INSERT Results (FormID, Taken, DataStream) OUTPUT Inserted.ResultID VALUES ('{0}','{1:yyyy-MM-dd HH:mm}', @DataStream)",
            formID_i, DateTime.Now);
        table_cmd = new SqlCommand(sqlCmd_s, DB_conn);
        table_cmd.Parameters.AddWithValue("@DataStream", dataStream_ay);
        resultID_i = Convert.ToInt32(table_cmd.ExecuteScalar());

        table_cmd.Dispose();
        DB_conn.Close();
        DB_conn.Dispose();
    } // end StoreResult

    /* Get stored survey results (there may be many) from DB based on survey name.
     */
    public List<string> GetResults(string surveyName_s)
    {
        int byteCount_i;                // Number of bytes used by the result stored in the field
        int bytesRead_i;                // Number of bytes read from the DB field
        SqlConnection DB_conn;          // Database connection
        int formID_i;                   // DB ID for a survey/form definition
        SqlDataReader listReader_dr;    // Reader for the selected survey results
        string resultFile_s;            // Survey result read from a file
        string selectCmd_s;             // Generated SQL SELECT command
        List<string> surveyList_ls;     // Generated and returned list of survey results
        SqlCommand table_cmd;           // SQL table command

        surveyList_ls = new List<string>();

        DB_conn = new SqlConnection(connString_cs);
        DB_conn.Open();
        selectCmd_s = String.Format("SELECT FormID FROM Forms WHERE FormName='{0}'", surveyName_s);
        table_cmd = new SqlCommand(selectCmd_s, DB_conn);
        formID_i = Convert.ToInt32(table_cmd.ExecuteScalar());

        selectCmd_s = String.Format("SELECT ResultID, DataStream FROM Results WHERE FormID='{0}'", formID_i);
        table_cmd = new SqlCommand(selectCmd_s, DB_conn);
        
        listReader_dr = table_cmd.ExecuteReader();
        while (listReader_dr.Read())
        {
            byteCount_i = Convert.ToInt32(listReader_dr.GetBytes(1, 0, null, 0, 0x7FFFFFFF)); // Get size of byte stream
            byte[] resultStream_ay = new byte[byteCount_i];     // Allocate buffer

            bytesRead_i = Convert.ToInt32(listReader_dr.GetBytes(1, 0, resultStream_ay, 0, byteCount_i));
            resultFile_s = Encoding.ASCII.GetString(resultStream_ay);
            if (bytesRead_i > 0) surveyList_ls.Add(resultFile_s);
        }

        table_cmd.Dispose();
        DB_conn.Close();
        DB_conn.Dispose();

        return surveyList_ls;
    } // end GetResults

    // === Utilities ===

    /* Add a note to the log file.
     */
    private void reportToLog_c(string message_s)
    {
        try { File.AppendAllText(logFilePath_cs, String.Format("{0:yyyy-MM-dd HH:mm} ", DateTime.Now) + message_s + "\n"); }
        catch { }
    } // end reportToLog_c

} // end StorageAccess
} // end namespace surveyjs-aspnet-mvc
